using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float movementSpeed = 2f;

    Vector2 mousePosition;

    bool moving;

    GameObject marker;


    /// <summary>
    /// Starts this instance.
    /// </summary>
    private void Start()
    {
        //sets marker to be the marker
        marker = GameObject.Find("PositionMarker");
    }

    /// <summary>
    /// Updates this instance.
    /// </summary>
    void Update()
    {

        if (Input.GetMouseButtonDown(0)) //gets click input
        {
            //get mouse position
            mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            moving = true; // changes moving to true

            marker.SetActive(true);                    //displays the marker
            marker.transform.position = mousePosition; //sets marker at to move location           

        }
        //checks if pouse clicked and object is not at mouse psotion
        if(moving && (Vector2)transform.position != mousePosition)
        {

            // gets the step amount for each frame
            float step = movementSpeed * Time.deltaTime;
            //calculates move towards psotions
            transform.position = Vector2.MoveTowards(transform.position, mousePosition, step);

        }
        else
        {
            //sets moving to false
            moving = false;
            marker.SetActive(false); //removes marker from be visible. 
        }

    }

}
